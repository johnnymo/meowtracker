import paho.mqtt.client as mqtt
import stmpy
import logging
from appJar import gui
from threading import Thread
import time
import json
from random import randint

#Choose mqtt broker (NTNU) and which port to connect
MQTT_BROKER = 'mqtt.item.ntnu.no'
MQTT_PORT = 1883

#Select which MQTT-Topic the cat door needs to push or pull from
MQTT_TOPIC_ALL = 'ttm4115/team_2/cat_door/#'
MQTT_TOPIC_RFID = 'ttm4115/team_2/cat_door/rfid'
MQTT_TOPIC_CAT = 'ttm4115/team_2/cat_door/cat'
MQTT_BE_REQ = 'ttm4115/team_2/cat_door/be_request'
MQTT_BE_RESPONS = 'ttm4115/team_2/cat_door/be_response'

#Local cache for valid cats.
local_cache = {
#    'Bowser': '1337' #Uncomment to add Bowser to local cache from the start without Back End (BE)
}
class MT_Door_STM:
    """
    State Machine for the cat door. The cat door image will change depending on what happens.
    Status: {'white': Idle, 'Yellow': Validating incoming RFID, 'red': Invalid RFID after BE consult, 'green': RFID }
    """
    
    def __init__(self):
        self._logger = logging.getLogger(__name__)
        print('logging under name {}.'.format(__name__))
        self.topic = ""
        self.payload = ""
        self.be_checked = False
        self.rfid = ""
        # Using a variable for the function handle_be_resp to use right away. When updating global variable local_cache there are some daley
        self.tempcache = {}
    def set_rfid(self, rfid):
        self.rfid = rfid
    def lock(self):
        self._logger.debug('Door locked')
        self.tempcache = {}
        app.setImage("Cat door IMG", "images\Idle.gif")
        app.setLabel("Status", "Idle")
        app.setLabelBg("Status", "white")
        app.setLabel("Lock", "Locked")
        app.setLabelBg("Lock", "red")
        app.setLabel("Door", "Closed")
        app.setLabelBg("Door", "red")
        self.be_checked = False #resetting variable to see if BE is consulted
    def unlock(self):
        self._logger.debug('Door unlocked')
        app.setImage("Cat door IMG", "images\Validated.gif")
        app.setLabel("Status", "RFID Valid")
        app.setLabelBg("Status", "green")
        app.setLabel("Lock", "Unlocked")
        app.setLabelBg("Lock", "green")
    def open_door(self):
        self._logger.debug('Door opened')
        app.setImage("Cat door IMG", "images\Open.gif")
        app.setLabel("Door", "Open")
        app.setLabelBg("Door", "green")
        cat_respons()
    def check_cache(self): 
        self._logger.debug('Validating RFID: Checking Local Cache')
        app.setImage("Cat door IMG", "images\Validating.gif")
        if not self.be_checked:
            app.setLabel("Status", "Validating: Checking Local Cache")
        app.setLabelBg("Status", "yellow")
        rfid = self.rfid
        time.sleep(2)
        for key, value in local_cache.items():
            if str(value) == rfid:
                self._logger.debug('Valid RFID: {} read'.format(value))
                driver.send('rfid_valid', 'mt_door_stm')
                return
        if not self.be_checked:
            self._logger.debug('RFID: {} not In Cache. Checking Back End'.format(rfid))
            driver.send('not_in_cache', 'mt_door_stm')
        elif self.be_checked:
            self._logger.debug('RFID: {} not Authorized'.format(rfid))
            driver.send('rfid_invalid', 'mt_door_stm')
            app.setImage("Cat door IMG", "images\Invalid.gif")
            app.setLabel("Status", "RFID Invalid")
            app.setLabelBg("Status", "red")
            time.sleep(4)

    def ask_be(self):
        self._logger.debug('Asking Back End on RFID: {}'.format(self.rfid))
        app.setLabel("Status", "Validating: Checking BE")
        payload = self.rfid
        mqtt_client.mqtt_client.publish(MQTT_BE_REQ, '{}'.format(payload))
        #Give the BackEnd time to respond
        time.sleep(2)
        self.be_checked = True #BE is consulted
    
    def report_be(self):
        rfid = self.rfid
        report = {'Door': str(rfid)}
        jdump = json.dumps(report)
        mqtt_client.mqtt_client.publish(MQTT_BE_REQ, jdump)
    
    def report_be_no_enter(self):
        rfid = self.rfid
        report = {'Door': 'no_enter', 'RFID': str(rfid)}
        jdump = json.dumps(report)
        mqtt_client.mqtt_client.publish(MQTT_BE_REQ, jdump)

    

class MQTT_Client:
    def on_connect(self, client, userdata, flags, rc):
        """
        This function tells what to do when connecting to MQTT-Broker
        """
        #log when connected to broker
        self._logger.debug('Cat Door connected to broker: {}'.format(client))
        
    def on_message(self, client, userdata, msg):
        """
        This function will process the incoming mqtt messages and give the correct
        message to the cat door state machine which handle further processes.
        """
        #Log which topic messages enters
        self._logger.debug('Incoming message to topic {} in Cat Door'.format(msg.topic))
        self._logger.debug('Payload: {}'.format(msg.payload))
        mt_door_stm.topic = msg.topic
        topic = mt_door_stm.topic
        mt_door_stm.payload = msg.payload.decode("utf-8")
        payload = mt_door_stm.payload
        #Handle RFID from cats 
        if MQTT_TOPIC_RFID in topic:
            #Sending message to stm that it has received an RFID-tag in the reader
            driver.send('receive_RFID', 'mt_door_stm', args=[payload])
        #Handle cats actions with MQTT
        elif MQTT_TOPIC_CAT in topic:
            if "enter" in payload:
                driver.send('in_frame', 'mt_door_stm')
            elif "out" in payload:
                driver.send('closed_door', 'mt_door_stm')
            else: 
                self._logger.info('Cats do crazy things, for example {}'.format(payload))
        #Handle responses from BE server
        elif MQTT_BE_RESPONS in topic:
            self._logger.debug('Handling Response from Back End server')
            jdict = json.loads(payload)
            handle_be_resp(jdict)
            
        else:
            self._logger.debug('Topic: {} is not a handled topic in cat door'.format(msg.topic))
            

    def __init__(self):
        # get the logger object for the Cat Door
        self._logger = logging.getLogger(__name__)
        print('logging under name {}.'.format(__name__))
        self._logger.info('Starting Cat Door')

        #establishing MQTT client
        self._logger.debug('Connecting to MQTT broker {} at port {}'.format(MQTT_BROKER, MQTT_PORT))
        self.mqtt_client = mqtt.Client()
        # callback methods
        self.mqtt_client.on_connect = self.on_connect
        self.mqtt_client.on_message = self.on_message
        # Connect to the broker defined in the beginning of code
        self.mqtt_client.connect(MQTT_BROKER, MQTT_PORT)
        # subscribe to topic to see cat status and rfid reader
        self.mqtt_client.subscribe(MQTT_TOPIC_CAT)
        self.mqtt_client.subscribe(MQTT_TOPIC_RFID)
        self.mqtt_client.subscribe(MQTT_BE_RESPONS)
        # uncomment below to subscribe to every Cat Door topic
        # self.mqtt_client.subscribe(MQTT_TOPIC_ALL)
        # start the internal loop to process MQTT messages
        self.mqtt_client.loop_start()
    
    def stop(self):
        """
        Stopping the Cat door smoothly.
        """
        # stop the MQTT client
        self.mqtt_client.loop_stop()
        # stop the state machine Driver
        driver.stop()
        exit(0)

#Function to handle BE respons to Cat door
def handle_be_resp(jdict):
    try:
        func = jdict['Func']
    except:
        return
    if 'add' in func:
        cat = jdict['Cat']
        rfid = jdict['RFID']
        local_cache[str(cat)] = str(rfid)
    if 'remove' in func:
        cat = jdict['Cat']
        rfid = jdict['RFID']
        local_cache.pop(str(cat))
    if 'update' in func:
        #This function will handle updates from database on the entire local cache
        pass
    #print(str(local_cache))

def cat_respons():
    """
    This function simulate the cats time inside the door before it exits. This is to simplify the simulation.
    """
    time.sleep(randint(4,8))
    mqtt_client.mqtt_client.publish(MQTT_TOPIC_CAT, 'out')

# defining transitions
t0 = {
    'source': 'initial',
    'target': 'idle'
}
t1 = {
    'trigger': 'receive_RFID',
    'source': 'idle',
    'target': 'validation',
    'effect': 'set_rfid(*), start_timer("t_vld", 1000)'
}
t2 = {
    'trigger': 't_vld',
    'source': 'validation',
    'target': 'idle'
}
t3 = {
    'trigger': 'rfid_invalid',
    'source': 'validation',
    'target': 'idle'
}
t4 = {
    'trigger': 'not_in_cache',
    'source': 'validation',
    'target': 'validation',
    'effect': 'ask_be()'
}
t5 = {
    'trigger': 'rfid_valid',
    'source': 'validation',
    'target': 'unlocked',
    'effect': 'unlock()'
}
t6 = {
    'trigger': 't_lock',
    'source': 'unlocked',
    'target': 'idle',
    'effect': 'report_be_no_enter()'
}
t7 = {
    'trigger': 'in_frame',
    'source': 'unlocked',
    'target': 'open',
    'effect': 'stop_timer("t_lock")'
}
t8 = {
    'trigger': 'closed_door',
    'source': 'open',
    'target': 'idle',
    'effect': 'report_be()'
}
#defining states
s0 = {
    'name': 'idle',
    'entry': 'lock()'
}
s1 = {
    'name': 'validation',
    'entry': 'check_cache()',
    'exit': 'stop_timer("t_vld")'
}
s2 = {
    'name': 'unlocked',
    'entry': 'start_timer("t_lock", 5000)' 
}
s3 = {
    'name': 'open',
    'entry': 'open_door()'
}

#initialize GUI and adding image
app = gui("Cat Door")
app.addLabel("Status", "Idle")
app.setLabelBg("Status", "white")
app.addHorizontalSeparator()
app.addLabel("LockLabl", "Lock:")
app.addLabel("Lock", "Locked")
app.setLabelBg("Lock", "red")
app.addHorizontalSeparator()
app.addLabel("DoorLabl", "Door:")
app.addLabel("Door", "Closed")
app.setLabelBg("Door", "red")
app.addHorizontalSeparator()
app.addImage("Cat door IMG", "images\Idle.gif")
#Initializing MQTT client object
mqtt_client = MQTT_Client()

#Creating state machine with the class MT_DOOR_STM as object
mt_door_stm = MT_Door_STM()
mt_door_machine = stmpy.Machine(transitions=[t0, t1, t2, t3, t4, t5, t6, t7, t8], obj= mt_door_stm, name='mt_door_stm', states=[s0, s1, s2, s3])
mt_door_stm.stm = mt_door_machine

#adding state machine to driver and starting driver
driver = stmpy.Driver()
driver.add_machine(mt_door_machine)
driver.start()
# copy of logger from jupyter notebook
# logging.DEBUG: Most fine-grained logging, printing everything
# logging.INFO:  Only the most important informational log items
# logging.WARN:  Show only warnings and errors.
# logging.ERROR: Show only error messages.
debug_level = logging.DEBUG
logger = logging.getLogger(__name__)
logger.setLevel(debug_level)
ch = logging.StreamHandler()
ch.setLevel(debug_level)
formatter = logging.Formatter('%(asctime)s - %(name)-12s - %(levelname)-8s - %(message)s')
ch.setFormatter(formatter)
logger.addHandler(ch)


#Start GUI and add a shutdown button
app.addButton("Shutdown", mqtt_client.stop)
app.setStopFunction(mqtt_client.stop)
app.go()