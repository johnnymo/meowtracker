from appJar import gui
import paho.mqtt.client as mqtt
import json
import ast


MQTT_BROKER                         = 'mqtt.item.ntnu.no'
#MQTT_BROKER                        = 'localhost'
MQTT_PORT                           = 1883
MQTT_TOPIC                          = 'ttm4115/team_2/#'
MQTT_TOPIC_SETTINGS                 = 'ttm4115/team_2/settings'
MQTT_TOPIC_RETRIEVE_SETTINGS        = 'ttm4115/team_2/retrieve_settings'
MQTT_TOPIC_UPDATE_SETINGS           = 'ttm4115/team_2/update_settings'
MQTT_TOPIC_GET_HISTORIC_POSITION    = 'ttm4115/team_2/historic'
MQTT_TOPIC_ALERT                    = 'ttm4115/team_2/alert'
MQTT_TOPIC_ALERT_CAT_DOOR           = 'ttm4115/team_2/cat_door/alert'
MQTT_TOPIC_LOCATION_HISTORY         = 'ttm4115/team_2/history'

history = ()


class MQTT_Client:

    def __init__(self):
        self.mqtt_client = mqtt.Client()
        self.mqtt_client.connect(MQTT_BROKER, MQTT_PORT)
        self.mqtt_client.subscribe(MQTT_TOPIC_LOCATION_HISTORY)
        self.mqtt_client.subscribe(MQTT_TOPIC_ALERT)
        self.mqtt_client.subscribe(MQTT_TOPIC_ALERT_CAT_DOOR)
        self.mqtt_client.subscribe(MQTT_TOPIC_SETTINGS)
        self.mqtt_client.on_message = self.on_message

        self.mqtt_client.loop_start()
    
    def on_message(self, client, userdata, msg):
        payload = msg.payload.decode('utf-8')
        payload_check = ast.literal_eval(payload)
        message_payload_receiver = json.loads(payload)
        print("Message received")

        if MQTT_TOPIC_ALERT in msg.topic:
            print('ALERT')
            alert = ''
            if 'battery_status' in payload_check:
                alert = 'Battery status of collar: {}'.format(message_payload_receiver['battery_status'])   
            elif 'direction' in payload_check:
                if message_payload_receiver['direction'] == 'home':
                    alert = 'Cat entered through cat door'
                elif message_payload_receiver['direction'] == 'out':
                    alert = 'Cat left through cat door'
                else:
                    alert = message_payload_receiver['direction']
            elif 'collar' in payload_check:
                alert = 'Collar is {}'.format(message_payload_receiver['collar'])
            elif 'cat' in payload_check:
                alert = 'Collar ID {} entered unsafe area'.format(message_payload_receiver['collarid'])
            app.addListItem('log_listbox', alert)

        elif MQTT_TOPIC_LOCATION_HISTORY in msg.topic:
            print('History')
            lines = []
            line = ''
            if len(message_payload_receiver) > 1:
                print('List of locations')
                for item in message_payload_receiver:
                    line = 'Collar ID: {}, Long: {}, Lat {}'.format(item[0], item[3], item[2])
                    lines.insert(0,line)
                app.clearListBox('history_listbox', callFunction=False)
                app.addListItems('history_listbox', lines)              
            else:
                print('Single location')
                lon = message_payload_receiver[0][3]
                lat = message_payload_receiver[0][2]               
                app.setLabel('longitudeval', lon)
                app.setLabel('Latitudeval', lat)

        elif MQTT_TOPIC_SETTINGS in msg.topic:
            print('Settings')
            lines = []
            line = ''
            lines.append('Collars:')
            for item in message_payload_receiver['collar']:
                line = 'Collar ID: {}, Name: {}, Bat. thres.: {}'.format(item[0], item[1], item[2])
                lines.append(line) 
            lines.append('Doors:')
            for item in message_payload_receiver['door']:
                line = 'Collar ID: {}, Allowed: {}'.format(item[0], item[1])
                lines.append(line)
            lines.append('Areas:')
            for item in message_payload_receiver['areas']:
                line = 'Collar ID: {}, lllat: {}, lllon: {}, urlat: {}, urlon: {}, mode: {}'.format(item[0], item[1], item[2], item[3], item[4], item[5])
                lines.append(line)
            app.clearListBox('view_listbox', callFunction=False)
            app.addListItems('view_listbox', lines)

client = MQTT_Client()

def getCoord():
    print('Getting coordinates')
    collar_id = app.getEntry('gps_collarid_entry')
    data = {'collarid':   collar_id}
    payload = json.dumps(data)
    client.mqtt_client.publish(MQTT_TOPIC_GET_HISTORIC_POSITION, payload)

def getCoordHistory():
    print('Getting coordinate history')
    collar_id = app.getEntry('history_collarid_entry')
    items = app.getEntry('history_items_entry')
    data = {'collarid':   collar_id,
            'items':      items}
    payload = json.dumps(data)
    client.mqtt_client.publish(MQTT_TOPIC_GET_HISTORIC_POSITION, payload)

def updateThreshold():
    print('Updating cat')
    value = app.getEntry('cat_entry_threshold')
    collar_id = app.getEntry('cat_entry_collarid')
    name = app.getEntry('cat_entry_name')
    data = {
            'threshold':    value,
            'name':         name,
            'collarid':     collar_id       
    }
    payload = json.dumps(data)
    client.mqtt_client.publish(MQTT_TOPIC_UPDATE_SETINGS, payload)

def addWhitelist():
    print('Adding to whitelist')
    collar_id = app.getEntry('collarid_entry')
    data = {
            'Func':     'add',
            'collarid': collar_id
    }
    payload = json.dumps(data)
    client.mqtt_client.publish(MQTT_TOPIC_UPDATE_SETINGS, payload)

def removeWhitelist():
    print('Removing from whitelist')
    collar_id = app.getEntry('collarid_entry')
    data = {
            'Func':     'remove',
            'collarid': collar_id
    }
    payload = json.dumps(data)
    client.mqtt_client.publish(MQTT_TOPIC_UPDATE_SETINGS, payload)

#needed at all????
def addNewCat():
    print('MEOW :3')
    collar_id = app.getEntry('cat_entry_collarid')
    name = app.getEntry('cat_entry_name')
    threshold = app.getEntry('cat_entry_threshold')
    data = {
            'name':         name,
            'collarid':     collar_id,
            'threshold':    threshold
    }
    payload = json.dumps(data)
    client.mqtt_client.publish(MQTT_TOPIC_UPDATE_SETINGS, payload)

def addSafeArea():
    print('Adding area')
    collar_id = app.getEntry('fence_collar_entry')
    lllon = app.getEntry('fence_lllon_entry')
    lllat = app.getEntry('fence_lllat_entry')
    urlon = app.getEntry('fence_urlon_entry')
    urlat = app.getEntry('fence_urlat_entry')
    if app.getCheckBox('fence_mode_checkbox'):
        mode = 0
    else:
        mode = 1
    data = {
            'area':     mode,
            'collarID': collar_id,
            'lllat':    lllat,
            'lllon':    lllon,
            'urlat':    urlat,
            'urlon':    urlon,
            'mode':     mode
    }
    payload = json.dumps(data)
    client.mqtt_client.publish(MQTT_TOPIC_UPDATE_SETINGS, payload)

def getSettings():
    print('Getting settings')
    client.mqtt_client.publish(MQTT_TOPIC_RETRIEVE_SETTINGS)

with gui("Userdevice", "500x500") as app:

    with app.tabbedFrame('test', rowspan=8):
        with app.tab('GPS'):
            app.label('Longitude', row=0, column=0)
            app.addSelectableLabel('longitudeval', 'none', row=0, column=1)
            app.label('Latitude', row=1, column=0)
            app.addSelectableLabel('Latitudeval','none', row=1, column=1)
            app.label('gps_collarid_label', 'Collar ID', row=2, column=0)
            app.addEntry('gps_collarid_entry', row=2, column=1)
            app.addNamedButton('Get current coordinates', "curr_coord_btn", getCoord, row=3, column=1)

        with app.tab('GPS History'):
            app.addListBox('history_listbox', history, colspan=2)
            app.label('history_collarid_label', 'Collar ID', row=1, column=0)
            app.addEntry('history_collarid_entry', row=1, column =1)
            app.label('history_items_label', 'Entries to request', row=2, column=0)
            app.addEntry('history_items_entry', row=2, column=1)
            app.addNamedButton('Get history', 'history_btn', getCoordHistory, row=3, column=1)

        with app.tab('Adjust settings'):
            with app.labelFrame('Add cat'):
                app.label('cat_label_name', 'Name', row=0, column=0)
                app.entry('cat_entry_name', row=0, column=1)
                app.label('cat_label_threshold', 'Battery threshold',row=1, column=0)
                app.entry('cat_entry_threshold',row=1, column=1)
                app.label('cat_label_collarid', 'Collar ID' ,row=2, column=0)
                app.entry('cat_entry_collarid',row=2, column=1)
                app.addNamedButton('Update/add cat', 'update_btn', updateThreshold,row=3, column=1)
            with app.labelFrame('Whitelist'):
                app.label('whitelist_label_collarid','Collar ID', row=0, column=0)
                app.entry('collarid_entry', row=0, column=1)
                app.addNamedButton('Add', 'add_btn', addWhitelist,row=1, column=0)
                app.addNamedButton('Remove', 'remove_btn', removeWhitelist,row=1, column=1)
        
        with app.tab('View settings'):
            app.addListBox('view_listbox')
            app.addNamedButton('View settings', 'view_settings_btn', getSettings)

        with app.tab('Safe area'):
            with app.labelFrame('Geofence'):
                app.addLabel('fence_collar_label','Collar ID', row=0, column=0)
                app.addEntry('fence_collar_entry', row=0, column=1)
                app.addLabel('fence_lllon_label','Lower left longitude', row=1, column=0)
                app.addEntry('fence_lllon_entry', row=1, column=1)
                app.addLabel('fence_lllat_label','Lower left latitude', row=2, column=0)
                app.addEntry('fence_lllat_entry', row=2, column=1)
                app.addLabel('fence_urlon_label','Upper right longitude', row=3, column=0)
                app.addEntry('fence_urlon_entry', row=3, column=1)
                app.addLabel('fence_urlat_label','Upper right latitude', row=4, column=0)
                app.addEntry('fence_urlat_entry', row=4, column=1)
                app.addNamedCheckBox('Unsafe','fence_mode_checkbox')
                app.addNamedButton('Submit', 'safe_area_btn', addSafeArea, row=5, column=1)

        with app.tab('Log'):
            app.addListBox('log_listbox')
            




            