import paho.mqtt.client as mqtt
import json
from appJar import gui
from stmpy import Machine, Driver
import time

#Connectivity settings
MQTT_BROKER         = 'mqtt.item.ntnu.no'
#MQTT_BROKER         = 'localhost'

MQTT_PORT           = 1883
MQTT_TOPIC_BE       = 'ttm4115/team_2/be'
MQTT_TOPIC_COLLAR   = 'ttm4115/team_2/collar'

#For simulation purposes
MQTT_TOPIC_DOOR_APPROACH    = 'ttm4115/team_2/cat_door/rfid'
MQTT_TOPIC_DOOR_ENTER       = 'ttm4115/team_2/cat_door/cat'

#MT Collar start
class MTCollar:
    #Collar id, normally generated for each collar instance
    RFID = 1001
    
    #Accumulator management settings
    #User threshold should not be lower than a critical one; to be controlled on UE and BE sides
    critical_threshold  = 5
    user_threshold      = critical_threshold
    
    #Home mode indicator
    home_mode = False
    
    periodic_gps_updates    = True
    terminated              = False
    
  
    #Simulation of instant GPS data source; values to be updated via UI
    latitude    = 0
    longtitude  = 0
    
    #MT Accumulator as an integral part of MT Collar; simulates battery      
    class MTAccumulator:
        charge_level    = 100
        plugged_in      = False
        
        def uncharge(self):            
            print('Started uncharging. Current battery level =', self.charge_level)
            
            while (not mt_collar.terminated and not self.plugged_in and self.charge_level > 0):
                self.charge_level = self.charge_level - 1
                print('Uncharging... now ', self.charge_level)
                if self.charge_level == mt_collar.critical_threshold:
                    driver.send('threshold_reached', 'stm_collar', args=[True])
                elif self.charge_level == mt_collar.user_threshold:
                    driver.send('threshold_reached', 'stm_collar', args=[False])                    
                time.sleep(1)
                        
            if self.charge_level == 0:
                driver.send('no_charge', 'stm_collar')
                
            return
        
        def charge(self):           
            print('Started charging. Current battery level =', self.charge_level)            

            while (not mt_collar.terminated and self.plugged_in and self.charge_level < 100):
                self.charge_level = self.charge_level + 1
                print('Charging... now ', self.charge_level)
                time.sleep(1)

            return
        
        def set_plugged_in(self, is_plugged_in):
            self.plugged_in = is_plugged_in
            return
        
    #MT Accumulator end
                
    mt_accum = MTAccumulator()    
    
    #MT Accumulator states
    #done events only handled to supress warnings    
    uncharging  = {'name':      'uncharging',
                   'entry':     'set_plugged_in(False)',
                   'do':        'uncharge',
                   'done':      ''
    }  
    
    charging    = {'name':      'charging',
                   'entry':     'set_plugged_in(True)',
                   'do':        'charge',
                   'done':      ''
    }
        
    #MT Accumulator transitions    
    t0 = {'source': 'initial', 
          'target': 'uncharging'
    }
    
    t1 = {'trigger':    'plug_in', 
          'source':     'uncharging', 
          'target':     'charging',
    }
    
    t2 = {'trigger':    'plug_out', 
          'source':     'charging', 
          'target':     'uncharging',
    }
    
    #MT Accumulator STM initialization
    stm_accum = Machine(name='stm_accum', transitions=[t0, t1, t2], obj=mt_accum, states=[charging, uncharging])
             
    def set_periodic_gps_updates(self, periodic_gps_updates_enabled):
        self.periodic_gps_updates = periodic_gps_updates_enabled
        return
    
    def on_threshold_reached(self, is_critical_threshold):
        if is_critical_threshold:
            print("Critical theshold reached")
            data = {
                    "method":               "charge_notification",
                    "threshold_reached":    "critical",
                }
        else:
            print("User threshold reached. Enforcing power saving mode")
            data = {
                    "method":               "charge_notification",
                    "threshold_reached":    "user",
                }  
        payload = json.dumps(data)
        mqtt_client.mqtt_client.publish(MQTT_TOPIC_BE, payload)    
        
        return
    
    def update_ui(self, mode):
        if self.mt_accum.plugged_in:
            app.setMeterFill("charge_level_ui", "grey")
            app.setMeter("charge_level_ui", 100, text="Charging")
        elif self.mt_accum.charge_level > self.user_threshold:
            app.setMeterFill("charge_level_ui", "green")
            app.setMeter("charge_level_ui", 100, text="Sufficient charge")
        elif self.mt_accum.charge_level > self.critical_threshold:            
            app.setMeterFill("charge_level_ui", "yellow")
            app.setMeter("charge_level_ui", self.user_threshold, text="Charge below user threshold")
        elif self.mt_accum.charge_level > 0:           
            app.setMeterFill("charge_level_ui", "red")
            app.setMeter("charge_level_ui", self.critical_threshold, text="Critical charge")
        else:            
            app.setMeterFill("charge_level_ui", "black")
            app.setMeter("charge_level_ui", 0, text="Uncharged")
            
        app.setLabel("collar_mode_ui", mode)

    def choose_target_mode(self):
        if (not self.home_mode and self.mt_accum.charge_level > self.user_threshold):
            print("Choosing regular mode")
            return 'regular'
        elif self.mt_accum.charge_level > 0:
            print("Choosing powersaving mode")
            return 'powersaving'
        else:
            print("Choosing disabled mode")
            return 'disabled'
        
    def send_gps_updates(self):
        while (not self.terminated and self.periodic_gps_updates):
            self.send_gps_update()
            time.sleep(5)

        return
    
    def send_gps_update(self):
        data = {
                "method":       "gps_update",
                "latitude":     self.latitude,
                "longtitude":   self.longtitude,
                "rfid":         self.RFID
        }  
        payload = json.dumps(data)
        mqtt_client.mqtt_client.publish(MQTT_TOPIC_BE, payload)
        return
    
    def update_settings(self, new_user_threshold):
        new_user_threshold = int(new_user_threshold)

        if new_user_threshold >= self.critical_threshold:
            print("Updating user threshold from ", self.user_threshold, " to ", new_user_threshold)
            self.user_threshold = new_user_threshold
            
            data = {
                    "method": "settings_updated"
            }  
            payload = json.dumps(data)
            mqtt_client.mqtt_client.publish(MQTT_TOPIC_BE, payload)    
            
            if self.mt_accum.charge_level > self.user_threshold:
                driver.send('threshold_lowered', 'stm_collar')
            else:
                driver.send('threshold_reached', 'stm_collar', args=[False])  

        return
    
    def set_home_mode(self, home_mode_enabled):
        self.home_mode = home_mode_enabled
    
    def on_plug_event(self, plugged):
        self.mt_accum.plugged_in = plugged
                
        if plugged:
            self.stm_accum.send('plug_in')
        else:  
            self.stm_accum.send('plug_out')

        return   

#MT Collar end

mt_collar = MTCollar() 

# MT Collar states
#done events only handled to supress warnings        
regular = {'name':              'regular',
           'entry':             'update_ui("Regular mode"); set_periodic_gps_updates(True)',
           'exit':              'set_periodic_gps_updates(False)',
           'do':                'send_gps_updates',
           'request_gps':       'send_gps_update',
           'settings_update':   'update_settings(*)'
}
    
power_saving = {'name':             'powersaving',
                'entry':             'update_ui("Power Saving mode")',
                'request_gps':       'send_gps_update',
                'settings_update':   'update_settings(*)',
                'done':              ''

}
    
disabled = {'name':     'disabled',
            'entry':    'update_ui("Disabled")',
            'done':     ''                
}
       
#MT Collar transitions
# Regular mode: outgoing transitions
t0 = {'source': 'initial', 
      'target': 'regular'
}
    
t1 = {'trigger':    'threshold_reached', 
      'source':     'regular', 
      'target':     'powersaving',
      'effect':     'on_threshold_reached(*)'
}
    
t2 = {'trigger':    'home_mode_on', 
      'source':     'regular', 
      'target':     'powersaving',
      'effect':     'set_home_mode(True)'
}
    
t3 = {'trigger':    'plug_in', 
      'source':     'regular', 
      'target':     'disabled',
      'effect':     'on_plug_event(True)'
}
    
# PowerSaving mode: outgoing transitions
t4 = {'trigger':    'threshold_lowered', 
      'source':     'powersaving', 
      'target':     'regular'
}
    
t5 = {'trigger':    'home_mode_off', 
      'source':     'powersaving',
      'effect':     'set_home_mode(False)',
      'function':   mt_collar.choose_target_mode
}

t6 = {'trigger':    'threshold_reached', 
      'source':     'powersaving', 
      'target':     'powersaving', 
      'effect':     'on_threshold_reached(*)'
}
    
t7 = {'trigger':    'no_charge', 
      'source':     'powersaving', 
      'target':     'disabled'
}
    
t8 = {'trigger':    'plug_in', 
      'source':     'powersaving', 
      'target':     'disabled',
      'effect':     'on_plug_event(True)'
}
    
# Disabled: outgoing transitions
t9 = {'trigger':   'plug_out', 
      'source':     'disabled',
      'function':   mt_collar.choose_target_mode,
      'effect':     'on_plug_event(False)'
}

t10 = {'trigger':   'plug_in', 
      'source':     'disabled',
      'target':     'disabled',
      'effect':     'on_plug_event(True)'
}
 
#MT Collar STM initialization   
stm_collar = Machine(name='stm_collar', transitions=[t0, t1, t2, t3, t4, t5, t6, t7, t8, t9, t10], obj=mt_collar, states=[regular, power_saving, disabled])

driver = Driver()
driver.add_machine(stm_collar)
driver.add_machine(mt_collar.stm_accum)


#MQTT Client serving MT Collar
class MQTT_Client:
    
    def __init__(self):
        self.mqtt_client = mqtt.Client()
        self.mqtt_client.on_message = self.on_message
        self.mqtt_client.connect(MQTT_BROKER, MQTT_PORT)
        self.mqtt_client.subscribe(MQTT_TOPIC_COLLAR)
        self.mqtt_client.loop_start()
            
    def on_message(self, client, userdata, msg):
        print('Message received: '.format(msg.payload))
        
        payload = msg.payload.decode("utf-8")
        message_payload_receiver = json.loads(payload)
        method = message_payload_receiver['method']
        print('Method: ', method)

        if method == "set_home_mode":
            home_mode = message_payload_receiver['home_mode']
            print("Home mode: ", home_mode)
            if home_mode == "on":
                print("Turn on home mode")
                driver.send('home_mode_on', 'stm_collar')
            elif home_mode == "off":
                print("Turn off home mode")
                driver.send('home_mode_off', 'stm_collar')
            else:
                print("Invalid payload ignored")
        elif method == "update_settings":
            user_threshold = message_payload_receiver['user_threshold']
            driver.send('settings_update', 'stm_collar', args=[user_threshold])
        elif method == "get_gps_coordinates":
            print("GP request received")
            driver.send('request_gps', 'stm_collar')


mqtt_client = MQTT_Client()

def on_submit():
    mt_collar.latitude = app.getEntry("Longtitude")
    mt_collar.longtitude = app.getEntry("Latitude")
    return

def checked():
    plugged_in = app.getCheckBox('Plugged in')
    if plugged_in:
        driver.send('plug_in', 'stm_collar')
    else:
        driver.send('plug_out', 'stm_collar')
        
def on_exit():
    print('Shutting down...')
    mt_collar.terminated = True
    driver.stop()
    mqtt_client.mqtt_client.loop_stop()
    return True        

def cat_approach():
    mqtt_client.mqtt_client.publish(MQTT_TOPIC_DOOR_APPROACH, mt_collar.RFID)
def cat_enter():
    mqtt_client.mqtt_client.publish(MQTT_TOPIC_DOOR_ENTER, 'enter')

with gui("MT Collar", "500x500") as app:
    batterylevel = 76
        
    with app.labelFrame("Power", sticky='ew'):
        app.addCheckBox('Plugged in')
        app.setCheckBoxChangeFunction('Plugged in', checked)

        app.addMeter("charge_level_ui")

        app.addLabel("collar_mode_ui", "")

    with app.labelFrame("GPS"):
        app.addLabel("l_latitude", "Latitide", 0, 0)
        app.addNumericEntry("Latitude", 0, 1)
        app.addLabel("l_longtitude", "Longtitude", 1, 0)
        app.addNumericEntry("Longtitude", 1, 1) 
        app.buttons(["Submit"], on_submit)

    with app.labelFrame('RFID and movement', sticky='ew'):
        app.label("RFID tag = " + str(mt_collar.RFID), bg='blue', fg='white',)
        app.buttons(['Approach door', 'Enter door'], [cat_approach, cat_enter])
        
    app.setStopFunction(on_exit)
    
    driver.start()
