import paho.mqtt.client as mqtt
import logging
from threading import Thread
import json
from appJar import gui
import sqlite3
import stmpy
import ast
from time import time
import os

# MQTT connection details
MQTT_BROKER = 'mqtt.item.ntnu.no'
MQTT_PORT = 1883

MQTT_TOPIC_UPDATE_SETINGS = "ttm4115/team_2/update_settings"
MQTT_TOPIC_GET_SETINGS = "ttm4115/team_2/retrieve_settings"
MQTT_TOPIC_GET_CURRENT_POSITION = "ttm4115/team_2/askpos"
MQTT_TOPIC_GET_HISTORIC_POSITION = "ttm4115/team_2/historic"
MQTT_TOPIC_GET_DOOR_COMMS = "ttm4115/team_2/cat_door/be_request"
MQTT_TOPIC_DEFAULT_BE_COMMS = "ttm4115/team_2/be"


class DatabaseHandling:
    # Class for doing all the activities towards the MeowTracker database
    def __init__(self):
        # check if the MeowTracker database and its tables are created, creates it otherwise
        #
        # tables:
        #
        # positions(runningnr Primary Key, collarID, time, lat, lon)
        #
        # users_cats(runningnr Primary Key, appID, collarID)
        #
        # doorwhitelist(runningnr Primary Key, doorID, collarID, allowed)
        #
        # appids(AppID Primary Key, username, datecreated)
        #
        # collarids(CollarID Primary Key, catname, battery_threshold, datecreated, home) #home represent if cat is outside or inside
        #
        # doorids(DoorID Primary Key, name, datecreated)
        #
        # areas(areaid Primary Key, CollarID, AppID, leftlat, leftlon, rightlat, rightlon, mode) (gives a square from left lower pos to right/highest point of a square) (mode is either True/1 or False/0 -> safe area or unsafe area)
        #
        conncursor = False
        try:
            conn = sqlite3.connect("meowtracker.db")
            conncursor = conn.cursor()

            # creates the tables used in MeowTracker
            try:
                # creates table positions
                createpositiontable = """ CREATE TABLE IF NOT EXISTS positions (
                                            runningnr integer PRIMARY KEY,
                                            collarid integer NOT NULL,
                                            time real,
                                            lat real,
                                            lon real
                                        ); """
                conncursor.execute(createpositiontable)
            except Exception as f:
                logging.error(f)

            try:
                # creates table users_cats
                createuserconnectiontable = """ CREATE TABLE IF NOT EXISTS users_cats (
                                            runningnr integer PRIMARY KEY,
                                            appid integer NOT NULL,
                                            collarid integer NOT NULL
                                        ); """
                conncursor.execute(createuserconnectiontable)
            except Exception as f:
                logging.error(f)

            try:
                # creates table doorwhitelist
                # a cat/collarID is allowed with value 1 in allowed field
                createdoorwhitelisttable = """ CREATE TABLE IF NOT EXISTS doorwhitelist (
                                            runningnr integer PRIMARY KEY,
                                            doorid integer NOT NULL,
                                            collarid integer NOT NULL,
                                            allowed BOOLEAN NOT NULL CHECK (allowed IN (0,1))
                                        ); """
                conncursor.execute(createdoorwhitelisttable)
            except Exception as f:
                logging.error(f)

            try:
                # creates table appids
                createappidtable = """ CREATE TABLE IF NOT EXISTS appids (
                                            appid integer PRIMARY KEY,
                                            name text,
                                            datecreated integer
                                        ); """
                conncursor.execute(createappidtable)
            except Exception as f:
                logging.error(f)

            try:
                # creates table collarids
                # battery threshold is a value where the collar will switch state to lower/increase power usage++
                # home 1 = cat is inside house, home 0 = cat is outside
                createcollaridtable = """ CREATE TABLE IF NOT EXISTS collarids (
                                            collarid integer PRIMARY KEY,
                                            name text,
                                            battery_threshold integer,
                                            datecreated integer,
                                            home BOOLEAN NOT NULL CHECK (home IN (0,1))
                                        ); """
                conncursor.execute(createcollaridtable)
            except Exception as f:
                logging.error(f)

            try:
                # creates table doorids
                createdooridtable = """ CREATE TABLE IF NOT EXISTS doorids (
                                            doorid integer PRIMARY KEY,
                                            name text,
                                            datecreated integer
                                        ); """
                conncursor.execute(createdooridtable)
            except Exception as f:
                logging.error(f)

            try:
                # creates table areas
                # ll = lower left corner
                # ur = upper right corner
                # mode 1 is safe area, mode 0 is unsafe
                createareastable = """ CREATE TABLE IF NOT EXISTS areas (
                                            areaid integer PRIMARY KEY,
                                            collarid integer NOT NULL,
                                            appid integer NOT NULL,
                                            lllat real,
                                            lllon real,
                                            urlat real,
                                            urlon real,
                                            mode BOOLEAN NOT NULL CHECK (mode IN (0,1))
                                        ); """
                conncursor.execute(createareastable)
                conncursor.close()
                conn.close()
            except Exception as f:
                logging.error(f)

        except Exception as e:
            # fails to create table or database
            logging.error(e)

    def writepos(self, lat, lon, collarid=1):
        # writes position of collar to database
        epoch = time()
        try:
            lat = float(lat)
            lon = float(lon)
        except TypeError:
            logging.warning("pass lat/lon as floats!")

        execute_statement = """ INSERT INTO positions (collarid, time, lat, lon)  VALUES (?, ?, ?, ?);"""
        variables = (collarid, epoch, lat, lon)

        try:
            conn = sqlite3.connect("meowtracker.db")
            conncursor = conn.cursor()
            conncursor.execute(execute_statement, variables)
            conn.commit()
            conncursor.close()
            conn.close()
            return True
        except Exception as e:
            logging.warning(e)
            return False

    def appendwhitelist(self, collarid, doorID, allowed=1):
        # appends or removes a collar ID to white list for the cat door
        execute_statement = """ INSERT INTO doorwhitelist (doorid, collarid, allowed)  VALUES (?, ?, ?);"""
        variables = (doorID, collarid, allowed)

        try:
            conn = sqlite3.connect("meowtracker.db")
            conncursor = conn.cursor()
            conncursor.execute(execute_statement, variables)
            conn.commit()
            conncursor.close()
            conn.close()
            return True
        except Exception as e:
            logging.warning(e)
            return False

    def checkwhitelist(self, collarid=1):
        # checks if cat is allowed to pass through door
        execute_statement = """ SELECT allowed FROM doorwhitelist WHERE collarid = ? ORDER BY runningnr DESC LIMIT 1 ;"""
        variables = (int(collarid),)
        conn = sqlite3.connect("meowtracker.db")
        conncursor = conn.cursor()
        allowed = conncursor.execute(execute_statement, variables).fetchall()
        # history = conncursor.execute("SELECT * FROM positions").fetchall()
        conn.commit()
        conncursor.close()
        conn.close()
        if len(allowed) > 0:
            allowed = str(allowed).strip(",[()]'")
            allowed = int(allowed)
        try:
            if allowed == 1:
                return True
            else:
                return False
        except TypeError:
            logging.info("no whitelist entry for {}".format(collarid))

    def gethistory(self, collarid=1, itemsToRetrieve=1):
        # gets the position for a collarID. Specifies the number of points in the history that should be given to the user
        execute_statement = """ SELECT collarid,time,lat,lon FROM positions WHERE collarid = ? ORDER BY time DESC LIMIT ?;"""
        variables = (collarid, itemsToRetrieve)
        try:
            conn = sqlite3.connect("meowtracker.db")
            conncursor = conn.cursor()
            history = conncursor.execute(execute_statement, variables).fetchall()
            # history = conncursor.execute("SELECT * FROM positions").fetchall()
            conn.commit()
            conncursor.close()
            conn.close()
            return history
        except Exception as e:
            logging.warning(e)
            return False

    def getcatname(self, collarid=1):
        # gets the name for a collarID
        execute_statement = """ SELECT name FROM collarids WHERE collarid = ?;"""
        variables = (collarid,)
        conn = sqlite3.connect("meowtracker.db")
        conncursor = conn.cursor()
        catname = conncursor.execute(execute_statement, variables).fetchall()
        # history = conncursor.execute("SELECT * FROM positions").fetchall()
        conn.commit()
        conncursor.close()
        conn.close()
        if len(catname) > 0:
            catname = str(catname).strip(",[()]'")
        # print(catname)
        return catname

    def getsettings(self, collarid=1, doorID=1, appID=1):
        # gets the settings for a given collar ID

        # get settings for collar
        collar = ""
        execute_statement = """ SELECT collarid, name, battery_threshold  FROM collarids;"""
        try:
            conn = sqlite3.connect("meowtracker.db")
            conncursor = conn.cursor()
            collar = conncursor.execute(execute_statement).fetchall()
            conn.commit()
            conncursor.close()
            conn.close()
        except Exception as e:
            logging.warning(e)

        # get settings for door whitelist
        doorwl = ""
        execute_statement = """ SELECT DISTINCT collarid, allowed FROM doorwhitelist ORDER BY collarid DESC;"""
        try:
            conn = sqlite3.connect("meowtracker.db")
            conncursor = conn.cursor()
            doorwl = conncursor.execute(execute_statement).fetchall()
            conn.commit()
            conncursor.close()
            conn.close()
        except Exception as e:
            logging.warning(e)

        # get settings for areas
        areas = ""
        execute_statement = """ SELECT collarid, lllat, lllon, urlat, urlon, mode FROM areas;"""
        try:
            conn = sqlite3.connect("meowtracker.db")
            conncursor = conn.cursor()
            areas = conncursor.execute(execute_statement).fetchall()
            conn.commit()
            conncursor.close()
            conn.close()
        except Exception as e:
            logging.warning(e)

        returnmsg = {"collar": collar, "door": doorwl, "areas": areas}
        return returnmsg

    def writeareasettings(self, collarid=1, appID=1, lllat=0.0, lllon=0.0, urlat=0.0, urlon=0.0, mode=1):
        # writes the settings for a collar ID. Invokes at least one function in ComponentHandling!
        execute_statement = """ INSERT INTO areas (collarid, appid, lllat, lllon, urlat, urlon, mode)  VALUES (?, ?, ?, ?, ?, ?, ?);"""
        variables = (collarid, appID, lllat, lllon, urlat, urlon, mode)

        try:
            conn = sqlite3.connect("meowtracker.db")
            conncursor = conn.cursor()
            conncursor.execute(execute_statement, variables)
            conn.commit()
            conncursor.close()
            conn.close()
            return True
        except Exception as e:
            logging.warning(e)
            return False

    def writecollarsettings(self, collarid=1, name="noname", threshold=50, home=1):
        # writes the settings for a collar ID. Invokes at least one function in ComponentHandling!
        epoch = time()
        execute_statement = """ INSERT INTO collarids (collarid, name, battery_threshold, datecreated, home)  VALUES (?, ?, ?, ?, ?);"""
        variables = (collarid, name, threshold, epoch, home)
        try:
            conn = sqlite3.connect("meowtracker.db")
            conncursor = conn.cursor()
            conncursor.execute(execute_statement, variables)
            conn.commit()
            conncursor.close()
            conn.close()
            return True
        except Exception as e:
            logging.warning(e)
            return False

    def changecatdirection(self, collarid=1):
        # check and change cat direction when a cat walks through door
        direction = 0
        newdirection = 0
        returncode = False
        collar = ""
        execute_statement = """ SELECT DISTINCT home FROM collarids WHERE collarid = ? ORDER BY collarid DESC;"""
        variables = (collarid,)
        try:
            conn = sqlite3.connect("meowtracker.db")
            conncursor = conn.cursor()
            direction = conncursor.execute(execute_statement, variables).fetchall()
            conn.commit()
            conncursor.close()
            conn.close()
        except Exception as e:
            logging.warning(e)
        if len(direction) > 0:
            direction = str(direction).strip(",[()]'")
            # print(direction)
            logging.info("cat direction " + direction)
        if int(direction) == 0:
            newdirection = 1
            returncode = True
        else:
            newdirection = 0
            returncode = False

        execute_statement = """ UPDATE collarids SET home = ? WHERE collarid = ?;"""
        variables = (newdirection, collarid)
        try:
            conn = sqlite3.connect("meowtracker.db")
            conncursor = conn.cursor()
            conncursor.execute(execute_statement, variables)
            conn.commit()
            conncursor.close()
            conn.close()
        except Exception as e:
            logging.warning(e)
        return returncode

    def checkarea(self, lat, lon, collarid=1):
        # checks if the cat has moved into a given area from the position. Returns which type of movement/area cat is in
        conn = sqlite3.connect("meowtracker.db")
        conncursor = conn.cursor()

        # get list of configured areas
        areas = ""
        execute_statement = """ SELECT lllat, lllon, urlat, urlon, mode FROM areas WHERE collarid = ?;"""
        try:
            values = (collarid,)
            areas = conncursor.execute(execute_statement, values).fetchall()
            conn.commit()
        except Exception as e:
            logging.warning(e)

        # get last position
        lastpos = ""
        execute_statement = """ SELECT lat, lon FROM positions WHERE collarid = ? ORDER BY runningnr DESC LIMIT 1 OFFSET 1;"""

        try:
            variables = (collarid,)
            lastpos = conncursor.execute(execute_statement, variables).fetchall()
            conn.commit()
            conncursor.close()
            conn.close()
            # print(lastpos)
            lastpos = str(lastpos[0]).strip("()")
            lastlat, lastlon = lastpos.split(",")
            lastlat = float(lastlat)
            lastlon = float(lastlon)
        except Exception as e:
            # logging.warning(e)
            lastlat = lat
            lastlon = lon

        # calculate returncode
        mode = "none"
        for area in areas:
            ret_value = str(area).strip("()")
            lllat, lllon, urlat, urlon, areacode = ret_value.split(",")
            lllat = float(lllat)
            lllon = float(lllon)
            urlat = float(urlat)
            urlon = float(urlon)
            areacode = int(areacode)
            if areacode is 1 and lat > lllat and lon > lllon and lat < urlat and lon < urlon:
                if areacode is 1 and lastlat > lllat and lastlon > lllon and lastlat < urlat and lastlon < urlon:
                    mode = "none"
                else:
                    mode = "safe"
                    break
            elif areacode is 0 and lat > lllat and lon > lllon and lat < urlat and lon < urlon:
                if areacode is 0 and lastlat > lllat and lastlon > lllon and lastlat < urlat and lastlon < urlon:
                    mode = "none"
                else:
                    mode = "unsafe"
                    break
            else:
                if lllat < lastlat < urlat and lllon < lastlon < urlon:
                    mode = "regular"
                else:
                    mode = "none"
        logging.info("cat " + str(collarid) + " changed mode to " + str(mode))
        return mode


class ComponentHandling:
    # class for communicating with the different components; user device, collar and door
    def __init__(self):
        self.database = DatabaseHandling()

    def updatecollar(self, msg):
        # updates battery threshold in the collar
        msgpayload_tmp = msg.payload.decode("UTF-8")
        msgpayload = ast.literal_eval(msgpayload_tmp)
        if "threshold" in msgpayload:
            threshold = msgpayload["threshold"]
            name = ""
            collarid = 1
            try:
                name = msgpayload["name"]
            except:
                pass
            try:
                collarid = msgpayload["collarid"]
            except:
                pass

            if int(threshold) > 5 and int(threshold) < 100:
                # threshold must be between 5 (hardcoded critical value in collar) and 100 which is a fully uploaded collar
                if name is not "":
                    self.database.writecollarsettings(threshold=threshold, name=name, collarid=collarid)
                else:
                    self.database.writecollarsettings(threshold=threshold, collarid=collarid)
            response = {}
            response["method"] = "update_settings"
            response["user_threshold"] = threshold
            response = json.dumps(response)
            logging.info(str(response))
            mqtt_client.client.publish('ttm4115/team_2/collar', str(response))
            # self.stm.driver.send('waitingforcollar', 'mt_backend_stm')
            # self.stm.driver.send('finished_processing_msg', 'mt_backend_stm')

    def updatedoor(self, msg):
        # updates the local cache in the cat door if a cat should be allowed/disallowed entrance
        msgpayload_tmp = msg.payload.decode("UTF-8")
        msgpayload = ast.literal_eval(msgpayload_tmp)
        if "Func" in msgpayload:
            mode = msgpayload["Func"]  # Func: add, remove or update
            # catname = msgpayload["mode"] # Cat: name
            collarid = msgpayload["collarid"]  # RFID: collarid
            allowed = 1
            if "add" in mode:
                allowed = 1
            elif "remove" in mode:
                allowed = 0
            self.database.appendwhitelist(collarid=collarid, doorID=1, allowed=allowed)
            catname = self.database.getcatname(collarid)
            response = {}
            response["Func"] = mode
            response["Cat"] = str(catname)
            response["RFID"] = collarid
            response = json.dumps(response)
            # print(str(response))
            response = json.dumps(response)
            logging.info(str(response))
            mqtt_client.client.publish('ttm4115/team_2/cat_door/be_response', response)
            # self.stm.driver.send('finished_processing_msg', 'mt_backend_stm')

    def updatebackend(self, msg):
        # updates settings for the collar and door that should be stored centrally in the database
        msgpayload_tmp = msg.payload.decode("UTF-8")
        msgpayload = ast.literal_eval(msgpayload_tmp)
        collarid = 1
        appID = 1
        lllat = 0.0
        lllon = 0.0
        urlat = 0.0
        urlon = 0.0
        mode = 1
        catname = "noname"
        if "collarID" in msgpayload:
            collarid = msgpayload["collarID"]
        if "appID" in msgpayload:
            appID = msgpayload["appID"]
        if "lllat" in msgpayload:
            lllat = msgpayload["lllat"]
        if "lllon" in msgpayload:
            lllon = msgpayload["lllon"]
        if "urlat" in msgpayload:
            urlat = msgpayload["urlat"]
        if "urlon" in msgpayload:
            urlon = msgpayload["urlon"]
        if "mode" in msgpayload:
            mode = msgpayload["mode"]
        if "collarid" in msgpayload:
            collarid = msgpayload["collarid"]
        if "name" in msgpayload:
            catname = msgpayload["name"]  # name: catname
        if "catname" in msgpayload:
            catname = msgpayload["catname"]
        if "area" in msgpayload:
            mode = msgpayload["area"]
            try:
                if int(mode) != 0 or int(mode) != 1:
                    mode = msgpayload["mode"]
            except:
                logging.warning("Should use either 0 or 1 as area")
            self.database.writeareasettings(mode=mode, collarid=collarid, appID=appID, lllat=lllat, lllon=lllon,
                                            urlat=urlat, urlon=urlon)
        if "newcat" in msgpayload:
            catname = msgpayload["newcat"]
        if "threshold" in msgpayload:
            threshold = msgpayload["threshold"]
            self.database.writecollarsettings(collarid=collarid, name=catname, threshold=threshold)
        else:
            self.database.writecollarsettings(collarid=collarid, name=catname)

    def showsettings(self, msg):
        # shows settings to user
        msgpayload_tmp = msg.payload.decode("UTF-8")
        try:
            msgpayload = ast.literal_eval(msgpayload_tmp)
        except:
            pass
        settings = self.database.getsettings()
        logging.info("settings: " + str(settings))
        settings = json.dumps(settings)
        logging.info(str(settings))
        mqtt_client.client.publish('ttm4115/team_2/settings', settings)

    def rcvcatdoor(self, msg):
        # receive msg from cat door about movement in/out of door or cat not in cache
        # alerts user/collar depending on the movement

        msgpayload_tmp = msg.payload.decode("UTF-8")
        msgpayload = ast.literal_eval(msgpayload_tmp)
        response = False
        # print("catdoor")
        if type(msgpayload) is int:
            collarid = int(msgpayload)
            # print(collarid)
            check = self.database.checkwhitelist(collarid)
            # print(check)
            if check:
                response = {}
                response['Func'] = 'add'
                response['RFID'] = collarid
                response['Cat'] = self.database.getcatname(collarid)
                response = json.dumps(response)
                logging.info(str(response))
                mqtt_client.client.publish('ttm4115/team_2/cat_door/be_response', response)
            else:
                response = {}
                response['Func'] = 'remove'
                response['RFID'] = collarid
                response['Cat'] = self.database.getcatname(collarid)
                response = json.dumps(response)
                logging.info(str(response))
                mqtt_client.client.publish('ttm4115/team_2/cat_door/be_response', response)

        elif "Door" in msgpayload:
            id = msgpayload["Door"]
            if "no_enter" in id:
                # gets notified if cat triggered opening of door, but never entered
                id = msgpayload["RFID"]
                response = {}
                response["direction"] = "Warning: " + id + " opened, but did not enter through door"
                response = json.dumps(response)
                mqtt_client.client.publish('ttm4115/team_2/alert', response)
            else:
                # Cat moved through door. calculates which direction it went
                direction_resp = ""
                if "collarID" in msgpayload:
                    collarid = msgpayload["collarID"]
                    direction = self.database.changecatdirection(collarid=collarid)
                else:
                    direction = self.database.changecatdirection(collarid=id)
                if direction:
                    direction_resp = "home"
                    home_mode = "on"
                else:
                    direction_resp = "out"
                    home_mode = "off"
                response = {}
                response["direction"] = direction_resp
                response = json.dumps(response)
                response2 = {}
                response2["method"] = "set_home_mode"
                response2["home_mode"] = home_mode
                response2 = json.dumps(response2)
                logging.info(str(response))
                logging.info(str(response2))
                mqtt_client.client.publish('ttm4115/team_2/alert', response)
                mqtt_client.client.publish('ttm4115/team_2/collar', response2)

    def rcvpos(self, msg):
        # receive position. Check in db if cat is in/out of area borders

        msgpayload_tmp = msg.payload.decode("UTF-8")
        msgpayload = ast.literal_eval(msgpayload_tmp)
        response = False
        method = ""
        collarid = -1
        try:
            if "method" in msgpayload:
                method = msgpayload["method"]
        except TypeError:
            pass
        if "gps_update" in method:
            lat = ""
            lon = ""

            try:
                # collarid = msgpayload["collarid"]
                lat = msgpayload["latitude"]
                lon = msgpayload["longtitude"]
                try:
                    collarid = msgpayload["rfid"]
                except:
                    collarid = 1
                try:
                    # response is added so the user in the future can be alerted if there is an error logging the position
                    response = self.database.writepos(lat=lat, lon=lon, collarid=collarid)
                except Exception as f:
                    logging.warning(f)
            except Exception as e:
                logging.warning(e)

            # check if cat is outside area
            # print("check area")
            logging.info("check area")
            if int(collarid) != -1:
                changed_area = self.database.checkarea(lat=lat, lon=lon, collarid=collarid)
            else:
                changed_area = self.database.checkarea(lat=lat, lon=lon)
            logging.info(changed_area)
            if "unsafe" in str(changed_area):
                response = {}
                response["cat"] = "unsafe_area"
                response["collarid"] = collarid
                response = json.dumps(response)
                logging.info(str(response))
                response2 = {}
                response2["method"] = "set_home_mode"
                response2["home_mode"] = "off"
                response2 = json.dumps(response2)
                mqtt_client.client.publish('ttm4115/team_2/collar', response2)
                mqtt_client.client.publish('ttm4115/team_2/alert', response)
            elif "safe" in str(changed_area):
                response = {}
                response["method"] = "set_home_mode"
                response["home_mode"] = "on"
                response = json.dumps(response)
                logging.info(str(response))
                mqtt_client.client.publish('ttm4115/team_2/collar', response)
            elif "regular" in str(changed_area):
                response = {}
                response["method"] = "set_home_mode"
                response["home_mode"] = "off"
                response = json.dumps(response)
                logging.info(str(response))
                mqtt_client.client.publish('ttm4115/team_2/collar', response)
            elif "none" in str(changed_area):
                pass

            # self.stm.driver.send('finished_processing_msg', 'mt_backend_stm')
        elif "charge_notification" in method:
            try:
                threshold = msgpayload["threshold_reached"]
                if "user" in threshold:
                    threshold = "collar reached user specified threshold"
                msg = {}
                msg['battery_status'] = threshold
                msg = json.dumps(msg)
                logging.info(str(msg))
                mqtt_client.client.publish('ttm4115/team_2/alert', msg)
            except Exception as e:
                logging.warning(e)
            # self.stm.driver.send('finished_processing_msg', 'mt_backend_stm')

    def askpos(self, msg):
        # when asked by user, ask for position of collar.
        # Receives position by rcvpos function or alert user if timer in stm times out
        msgpayload_tmp = msg.payload.decode("UTF-8")
        msgpayload = ast.literal_eval(msgpayload_tmp)
        response = ""
        try:
            # collarid = msgpayload["collarid"]
            method = msgpayload["method"]
            if method == "getpos":
                if "collarid" in msgpayload:
                    collarid = msgpayload["collarid"]
                    msg = {}
                    msg['method'] = "get_gps_coordinates"
                    msg['collarid'] = collarid
                    msg = json.dumps(msg)
                    mqtt_client.client.publish('ttm4115/team_2/collar', msg)
                else:
                    msg = {}
                    msg['method'] = "get_gps_coordinates"
                    msg = json.dumps(msg)
                    mqtt_client.client.publish('ttm4115/team_2/collar', msg)
        except Exception as e:
            logging.warning(e)
        logging.info("waiting for position to return from collar")

    def askpos_regular(self):
        # when triggered by a timer the backend asks for the collars position.
        # This is done regularly to detect fraud passings in/out of door
        response = ""
        try:
            msg = {}
            msg['method'] = "get_gps_coordinates"
            msg = json.dumps(msg)
            mqtt_client.client.publish('ttm4115/team_2/collar', msg)
        except Exception as e:
            logging.warning(e)
        logging.info("sent regular position request")
        self.stm.driver.send('finished_processing_msg', 'mt_backend_stm')

    def notreturnedcollar(self):
        # alerts user when a collar doesnt return when asking
        logging.warning("collar didnt return")
        response = {}
        response["collar"] = "not responding"
        response = json.dumps(response)
        mqtt_client.client.publish('ttm4115/team_2/alert', response)
        logging.info(str(response))
        # self.stm.driver.send('finished_processing_msg', 'mt_backend_stm')

    def historicpos(self, msg):
        # retrieves the last n positions for a collar ID
        msgpayload_tmp = msg.payload.decode("UTF-8")
        msgpayload = ast.literal_eval(msgpayload_tmp)
        response = ""
        try:
            collarid = msgpayload["collarid"]
            itemstoretrieve = 0
            try:
                # check if user has specified nr of historic locations to retrieve. defaults to 1 (last position)
                itemstoretrieve = msgpayload["items"]
            except:
                pass
            try:
                # response is added so the user in the future can be alerted if there is an error logging the position
                if int(itemstoretrieve) > 1:
                    response = self.database.gethistory(collarid=collarid, itemsToRetrieve=itemstoretrieve)
                else:
                    response = self.database.gethistory(collarid=collarid)
            except Exception as f:
                logging.warning(f)
        except Exception as e:
            logging.warning(e)

        response = json.dumps(response)
        mqtt_client.client.publish('ttm4115/team_2/history', response)
        logging.info(str(response))
        # self.stm.driver.send('finished_processing_msg', 'mt_backend_stm')

    def distribute_processing(self, msg):
        # reads the message given from the stm triggered by a MQTT message
        if MQTT_TOPIC_UPDATE_SETINGS in msg.topic:
            self.updatebackend(msg)
            msgpayload_tmp = msg.payload.decode("UTF-8")
            msgpayload = ast.literal_eval(msgpayload_tmp)
            waiting = False
            if "Func" in msgpayload:
                self.updatedoor(msg)
            if "threshold" in msgpayload:
                self.updatecollar(msg)
                waiting = True
            if waiting:
                self.stm.driver.send('waitingforcollar', 'mt_backend_stm')
            else:
                self.stm.driver.send('finished_processing_msg', 'mt_backend_stm')

        elif MQTT_TOPIC_GET_SETINGS in msg.topic:
            self.showsettings(msg)
            self.stm.driver.send('finished_processing_msg', 'mt_backend_stm')

        elif MQTT_TOPIC_GET_CURRENT_POSITION in msg.topic:
            self.askpos(msg)
            self.stm.driver.send('waitingforcollar', 'mt_backend_stm')

        elif MQTT_TOPIC_GET_DOOR_COMMS in msg.topic:
            self.rcvcatdoor(msg)
            self.stm.driver.send('finished_processing_msg', 'mt_backend_stm')

        elif MQTT_TOPIC_DEFAULT_BE_COMMS in msg.topic:
            self.rcvpos(msg)
            self.stm.driver.send('finished_processing_msg', 'mt_backend_stm')

        elif MQTT_TOPIC_GET_HISTORIC_POSITION in msg.topic:
            self.historicpos(msg)
            self.stm.driver.send('finished_processing_msg', 'mt_backend_stm')


class MQTT_Client:
    # Handles the messages sent from the MQTT broker
    def __init__(self):
        self.count = 0
        self.client = mqtt.Client()
        self.client.on_connect = self.on_connect
        self.client.on_message = self.on_message

    def on_connect(self, client, userdata, flags, rc):
        # print('on_connect(): {}'.format(mqtt.connack_string(rc)))
        logging.info('on_connect(): {}'.format(mqtt.connack_string(rc)))

    def on_message(self, client, userdata, msg):
        logging.info('on_message(): topic: {}'.format(msg.topic))
        logging.info('on_message(): payload: {}'.format(msg.payload))

        self.stm_driver.send('start_processing', 'mt_backend_stm', args=[msg])
        """
        if MQTT_TOPIC_UPDATE_SETINGS in msg.topic:
            self.stm_driver.send('update_settings', 'mt_backend_stm', args=[msg])
        elif MQTT_TOPIC_GET_SETINGS in msg.topic:
            self.stm_driver.send('showsettings', 'mt_backend_stm', args=[msg])
        elif MQTT_TOPIC_GET_CURRENT_POSITION in msg.topic:
            self.stm_driver.send('askpos', 'mt_backend_stm', args=[msg])
        elif MQTT_TOPIC_GET_DOOR_COMMS in msg.topic:
            self.stm_driver.send('rcvcatdoor', 'mt_backend_stm', args=[msg])
        elif MQTT_TOPIC_DEFAULT_BE_COMMS in msg.topic:
            self.stm_driver.send('rcvpos', 'mt_backend_stm', args=[msg])
        elif MQTT_TOPIC_GET_HISTORIC_POSITION in msg.topic:
            self.stm_driver.send('historic', 'mt_backend_stm', args=[msg])
        """

    def start(self, broker, port):

        # print('Connecting to {}:{}'.format(broker, port))
        logging.info('Connecting to {}:{}'.format(broker, port))

        self.client.connect(broker, port)

        mqtt_update_settings = MQTT_TOPIC_UPDATE_SETINGS + "/#"
        mqtt_get_settings = MQTT_TOPIC_GET_SETINGS + "/#"
        mqtt_get_pos = MQTT_TOPIC_GET_CURRENT_POSITION + "/#"
        mqtt_get_door = MQTT_TOPIC_GET_DOOR_COMMS + "/#"
        mqtt_default_be = MQTT_TOPIC_DEFAULT_BE_COMMS + "/#"
        mqtt_get_history = MQTT_TOPIC_GET_HISTORIC_POSITION + "/#"

        logging.info("connected to " + mqtt_update_settings)
        logging.info("connected to " + mqtt_get_settings)
        logging.info("connected to " + mqtt_get_pos)
        logging.info("connected to " + mqtt_get_door)
        logging.info("connected to " + mqtt_default_be)
        logging.info("connected to " + mqtt_get_history)

        self.client.subscribe(mqtt_update_settings)
        self.client.subscribe(mqtt_get_settings)
        self.client.subscribe(mqtt_get_pos)
        self.client.subscribe(mqtt_get_door)
        self.client.subscribe(mqtt_default_be)
        self.client.subscribe(mqtt_get_history)
        self.client.subscribe("ttm4115/#")

        try:
            thread = Thread(target=self.client.loop_forever)
            thread.start()
        except KeyboardInterrupt:
            # print('Interrupted')
            logging.warning("Interrupted")
            self.client.disconnect()


if __name__ == '__main__':
    # implements the statemachine and starts the application

    # states
    s0 = {'name': 'idle',
          'entry': 'stop_timer("t_processing"); start_timer("t_askpos", "600000")'
          }
    s1 = {'name': 'processing',
          'entry': 'start_timer("t_processing", "3000")'
          }

    # transitions.
    # The messaging states could probably be put into one for simplicity, but requires some more coding when receiving
    # different messages
    t0 = {
        'source': 'initial',
        'target': 'idle'
    }
    t1 = {
        'trigger': 'finished_processing_msg',
        'source': 'processing',
        'target': 'idle',
        'effect': 'stop_timer("t_processing")'
    }
    t2 = {
        'trigger': 'waitingforcollar',
        'source': 'processing',
        'target': 'idle',
        'effect': 'start_timer("t_collarresponse", "3000")'
    }
    t3 = {
        'trigger': 'start_processing',
        'source': 'idle',
        'target': 'processing',
        'effect': 'distribute_processing(*); stop_timer("t_collarresponse")'
    }
    t4 = {
        'trigger': 't_processing',
        'source': 'processing',
        'target': 'idle'
    }
    t5 = {
        'trigger': 't_askpos',
        'source': 'idle',
        'target': 'processing',
        'effect': 'askpos_regular'
    }
    t6 = {
        'trigger': 'start_processing',
        'source': 'processing',
        'target': 'processing',
        'effect': 'distribute_processing(*); stop_timer("t_collarresponse")'
    }
    t7 = {
        'trigger': 't_collarresponse',
        'source': 'idle',
        'target': 'idle',
        'effect': 'notreturnedcollar'
    }
    t8 = {
        'trigger': 't_askpos',
        'source': 'processing',
        'target': 'processing',
        'effect': 'askpos_regular'
    }

    mt_be_stm_class = ComponentHandling()

    mt_be_stm = stmpy.Machine(
        transitions=[t0, t1, t2, t3, t4, t5, t6, t7, t8], obj=mt_be_stm_class,
        name="mt_backend_stm",
        states=[s0, s1])

    mt_be_stm_class.stm = mt_be_stm

    driver = stmpy.Driver()
    driver.add_machine(mt_be_stm)

    mqtt_client = MQTT_Client()
    mt_be_stm.mqtt_client = mqtt_client
    mqtt_client.stm_driver = driver

    # sets loglevel to INFO
    logging.basicConfig(level=os.environ.get("LOGLEVEL", "INFO"))

    # starts the MQTT client and the stm
    driver.start()
    mqtt_client.start(broker=MQTT_BROKER, port=MQTT_PORT)
