# MeowTracker

##### This is the **infamous** MeowTracker of Team 2! Now with GUI!

The MeowTracker is a health and safety aid for cat owners

Meowtracker consists of four components which need to be started separately.

These components are:
- The user device
- The cat collar
- The back end server
- The cat door

In the prototype all components uses MQTT protocol for communication. This was due to changes after the COVID-19 outbreak when every component became 100% GUI-based.

The MQTT broker used by all components is the NTNU broker: mqtt.item.ntnu.no